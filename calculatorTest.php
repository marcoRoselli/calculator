<?php

include_once ("calculator.php");

class calculatorTest extends PHPUnit_Framework_TestCase
{
	function setup () {
		$this->calc = new calculator();
	}
	
	/**
	* @dataProvider dataProvider
	**/
	function testCalc($input,$expected) {
		$result = $this->calc->leftToRight($input);
		$this->assertEquals($expected,$result);
	}
	
	
	
	public function dataProvider() {
		return array (
			array("0/2+3+5+6",14),
			array("1+1+1",3),
			array("2/0","Division by 0 is not allowed"),
			array("0/2+3+8-55",-44)
		);
	}
	
}	

<?php 

class calculator 
{
	function __construct() {
		$this->operators = ["+","-","/","*"];
	}
	/**
	 * @desc: transform string into array
	 * @param: (string) input from user
	 * @return: (array) values and operators
	 **/
	protected function string_to_array ($input) {
		$operators = $this->operators;
		$arrayVal = [];
		$index = 0;
		for ($k = 0; $k < strlen($input); $k++ ) {
			if (in_array($input[$k],$operators)) {
				$index++;
				$arrayVal[$index] = $input[$k];
				$index++;
			} else {
				if (array_key_exists($index,$arrayVal)) {
					$arrayVal[$index].= $input[$k];
				} else {
					$arrayVal[$index] = $input[$k];
				}
			}
		}
		return $arrayVal;
	}
	
	/**
	 * @desc: execute first the operations with priority
	 * @param: (array) values and operators
	 * @return: (array) values and operators without * and /
	 **/
	protected function precedence ($input) {
		$arrayVal = self::string_to_array($input);
		for ($k = 0; $k < count($arrayVal); $k++) {
			if ($arrayVal[$k] === "/" || $arrayVal[$k] === "*") {
				if ($arrayVal[$k] == "/" && $arrayVal[$k+1] == 0 ){
					return (false);
				}
				$arrayVal[$k] = self::make_calc($arrayVal[$k+1],$arrayVal[$k-1],$arrayVal[$k]);
				unset($arrayVal[$k-1]);
				unset($arrayVal[$k+1]);
				$k++;
			}
		}
		return $arrayVal;
	}
	
	/**
	 * @desc: execute what is left from left to right
	 * @param: (array) values and operators
	 * @return: (float) result
	 **/
	function leftToRight($input) {
		$operators = $this->operators;
		$arrayVal = self::precedence($input);
		if ($arrayVal == false) {
			return ("Division by 0 is not allowed");
		} else {
			$arrayVal = array_values($arrayVal);
		}
		$sum = (float)$arrayVal[0]; 
		$elementsCount = count($arrayVal);
		for ($k= 1; $k < $elementsCount;$k++) {
			if (!in_array($arrayVal[$k],$operators)) {
				$sum = self::make_calc($arrayVal[$k],$sum,$arrayVal[$k-1]);
			}
		}
		return ((float)$sum);
	}	
	
	private function make_calc($next,$prev,$operator) {
		switch ($operator) {
			case "*":
				$result = (float)$prev * (float)$next;
			break;
			case "/":
				$result = (float)$prev / (float)$next;
			break;
			case "+":
				$result = (float)$prev + (float)$next;
			break;
			case "-":
				$result = (float)$prev - (float)$next;
			break;
		}	
		return $result;
	}	
	
}


